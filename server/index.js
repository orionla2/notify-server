import Server from './app.js';

async function start() {
  return new Server();
}

module.exports = start();