import requestId from 'request-id/express';
import sessions from 'express-session';
import { EventEmitter } from 'events';
import bodyParser from 'body-parser';
import express from 'express';
import LoggerService from './services/logger.service.js';
import RedisService from './services/redis.service.js';
import NotifyService from './services/notify.service.js';

export default class Server extends EventEmitter {

  constructor(options) {
    super();

    this.app = express();
    this.app.use(bodyParser.json({
      limit: '20mb',
    }));

    this.app.use(requestId());
    this.app.use(sessions({
      secret: 'secret123',
      resave: true,
      saveUninitialized: true,
    }));

    // Define fsend.
    const self = this;
    this.app.use((req, res, next) => {
      req.startTime = Date.now();
      function _fsend(options, data) {
        const wait = Date.now() - req.startTime;
        const responseData = {
          data,
          time: new Date(),
          requestId: options && options.requestId,
          wait,
        };
        this.send(responseData);
      }
      res.fsend = _fsend.bind(res, { requestId: req.requestId });
      next();
    });

    this.router = new express.Router();
    this.prefix = '/v1';

    this.state = {
      isRunning: false,
      isStarting: false,
    };
    this.init();
  }

  async init() {
    this.state.isStarting = true;
    this.emit('status:starting');

    const logger = await LoggerService.create({
      console: true,
      level: "verbose",
      stack: true
    });
    this.logger = logger;

    const redis = new RedisService(this.logger);
    const notifyService = new NotifyService();
    notifyService.subscribe(() => {
      const now = Math.round((new Date()).getTime() / 1000);
      redis.find(`timers`, now, 0)
      .then(result => {
        result.filter((item) => {
          this.logger.verbose('[MESSAGE]', item);
          redis.remove(['timers', item]);
        });
      });
    });

    this.router.post('/echoAtTime', async (req, res, next) => {
      try {
        const ts = Math.round((new Date(req.body.time)).getTime() / 1000);

        await redis.add([`timers`, ts, JSON.stringify({timestamp: ts, message: req.body.message})]);
        res.status(200).fsend({status: 'OK'});
      } catch(err) {
        next(err);
      }
    });


    this.app.use(this.prefix, this.router);

    // Error handling.
    this.app.use((err, req, res, next) => {

      const error = {
        status: err.status,
        message: err.message,
        code: err.code || err.name,
      };
      const errors = err.errors;
      const status = error.status || 500;

      res.status(status).fsend({
        error,
        errors
      });

      if (status === 500) {
        this.logger.error(err.stack);
      }
    });

    // 404 error handling.
    this.app.use((req, res) => {
      res.status(404).fsend({
        error: {
          code: '404',
          message: 'Not found.',
        },
      });
    });

    await this.listen();
  }

  async listen() {
    return new Promise((resolve, reject) => {
      const port = 3000;
      this.app.listen(port, err => {
        if (err) {
          this.logger.info('Server can not start.');
          this.emit('error', err);
          reject(err);
        }
        this.logger.info(`Server has started at port ${port}`);

        this.state.isStarting = false;
        this.state.isRunning = true;
        this.emit('status:running');

        resolve();
      });
    });
  }

  async stop() {
  // TODO: implement.
  }
}