export default class NotifyService {
  constructor() {
    this.subject = Math.round((new Date()).getTime() / 1000);
    this.observers = [];
    setInterval(() => {
      this.subject = Math.round((new Date()).getTime() / 1000);
      this.notify();
    }, 1000);
  }

  notify() {
    this.observers.forEach(observer => {
      observer.call();
    });
  }

  subscribe(observer) {
    this.observers.push(observer);
  }

  unsubscribe(observer) {
    if(this.observers.indexOf(observer) !== -1) {
      this.observers.splice(this.observers.indexOf(observer), 1)
    }
  }
}