import redis from 'redis';
import _ from 'lodash';

export default class RedisService {
  constructor(logger) {
    this.logger = logger;
    this.db = redis.createClient({
      host: process.env.REDIS_HOST || '127.0.0.1',
      port: 6379,
      retry_strategy: (options) => {
        if (options.attempt > 5) return null;
        return Math.min(options.attempt * 100, 3000);
      }
    });
    this.lifetime = 300;
    this.db.on('connect', () => {
      this.logger.info('[REDIS::CONNECTED]');
    });

    this.db.on("error", (err) => {
      this.logger.info("Error " + err);
    });

    this.db.on("end", function() {
      this.logger.info("Redis connection closed!");
    });

  }

  async find(mask, max, min) {
    return await new Promise((resolve, reject) => {
      if (this.db.connected) {
        this.db.zrevrangebyscore([mask, max, min], (err, members) => {
          resolve(members);
        });
      } else {
        resolve(this.emptyReply());
      }
    });
  }

  async add(value) {
    if (this.db.connected) {
      return this.db.zadd(value);
    } else {
      return this.emptyReply();
    }
  }

  async remove(value) {
    if (this.db.connected) {
      return this.db.zrem(value);
    } else {
      return this.emptyReply();
    }
  }

  async get(mask) {
    return await new Promise((resolve, reject) => {
      if (this.db.connected) {
        this.db.get(mask, (err, reply) => {
          if(err) reject(err);
          resolve(reply)
        });
      } else {
        resolve(this.emptyReply());
      }
    });
  }

  async set(key, value) {
    if (this.db.connected) {
      return this.db.zadd(key, value);
    } else {
      return this.emptyReply();
    }
  }

  async delete(mask) {
    return await new Promise((resolve, reject) => {
      if (this.db.connected) {
        this.db.keys(mask, (err, rows) => {
          if (err) reject(err);
          rows.filter(row => {
            this.db.del(row);
            return false;
          })
        });
        resolve(true);
      } else {
        resolve(this.emptyReply());
      }
    });
  }

  async update(mask) {
    return await new Promise((resolve, reject) => {

    });
  }

  emptyReply() {
    return null;
  }
}