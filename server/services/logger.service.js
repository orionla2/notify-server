import winston from 'winston';

export default class LoggerService {
	static async create(options) {
		return new LoggerService(options);
	}
	constructor(_options) {
		const options = _options || {};
		const winsonLogger = new winston.Logger({
			level: options.level || 'verbose',
		});
		this.winsonLogger = winsonLogger;

		if (options.console) {
			winsonLogger.add(winston.transports.Console, {
				colorize: true,
				prettyPrint: true,
			});
		}

		[
			'verbose',
			'debug',
			'info',
			'warn',
			'error',
		].forEach(item => {
			this[item] = winsonLogger[item].bind(winsonLogger);
		});
	}
}
