#!/usr/bin/env bash
docker build -t ns_server:0.0.1 --rm -f Dockerfile-server .
docker build -t ns_nginx_proxy:0.0.1 --rm -f Dockerfile-proxy .