# notify-server

nodejs + redis

#Instalation

1. `cd ./server`
2. `npm i`

#Local strart

1. `cd ./server`
2. `npm run dev`

#Docker start

1. `./build-images.sh`
2. `./dev.sh`

#API collection

- https://www.getpostman.com/collections/e44114958dad7ccd61e1